
import java.util.Scanner;

public class Exercicio01 {

    public static void executar() {

        String nome = Prompt.lerLinha("Digite o nome:");
        double nota = Prompt.lerDecimal("Digite a nota:");

        Prompt.imprimir("Nome: " + nome);
        Prompt.imprimir("Nota: " + nota);

        // Scanner leitor = new Scanner(System.in);
        // System.out.println("Digite o nome");
        // String nome = leitor.nextLine();
        // System.out.println("Digite a nota");
        // double nota = leitor.nextDouble();
        // System.out.println("Nome: " + nome);
        // System.out.println("Nota: " + nota);
        // leitor.close();

    }

}
